$(document).ready(function() {
  chargerArtistes();
});

function chargerArtistes() {
  $.get('classmusique.php', function (rep) {
    $('#divArt').html(rep);
    $('#musique').change(function() {
      chargerMusique($(this).val());
    });
  });
}

function chargerMusique (idA) {
  $.get('songs.php?idArt=' + idA, function (rep) {
    $('#divSongs').html(rep);
  });
}

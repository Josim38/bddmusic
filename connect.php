<?php
try
{
// On se connecte à MySQL : données de connexion au serveur, puis à la base, puis utilisateur + mot de passe
$bdd = new PDO('mysql:host=localhost;dbname=mymusique','root','');
}
catch (Exception $e)
{
// En cas d'erreur, on affiche un message et on arrête tout (page plantée)
    die('Erreur : ' . $e->getMessage());
}

// Etablir la communication entre PHP et Mysql en UTF-8
$bdd->query("SET NAMES 'utf8'");

 ?>

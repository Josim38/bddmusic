<?php require_once('connect.php') ?>
<?php
$requete = $bdd->query('SELECT * FROM class_artistes');
?>
<select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="musique">
<option selected>Sélectionnez...</option>
<?php
while ($donnees = $requete->fetch()) { ?>
<option value="<?php echo($donnees['id_artiste']) ?>">
<?php echo($donnees['nom_artiste']) ?>
</option>
<?php } ?>
</select>

-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 06 Septembre 2017 à 08:53
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mymusique`
--

-- --------------------------------------------------------

--
-- Structure de la table `class_artistes`
--

CREATE TABLE `class_artistes` (
  `id_artiste` int(11) NOT NULL,
  `nom_artiste` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `class_artistes`
--

INSERT INTO `class_artistes` (`id_artiste`, `nom_artiste`) VALUES
(1, 'David Bowie'),
(2, 'Joy Division'),
(3, 'Bob Dylan'),
(4, 'Coldplay'),
(5, 'U2'),
(6, 'Castlevania (OST)'),
(7, 'Les Sheriffs');

-- --------------------------------------------------------

--
-- Structure de la table `song_musique`
--

CREATE TABLE `song_musique` (
  `id_song` int(11) NOT NULL,
  `titre_song` varchar(50) NOT NULL,
  `annee_sortie` year(4) NOT NULL,
  `id_artiste` int(11) NOT NULL,
  `id_genre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `song_musique`
--

INSERT INTO `song_musique` (`id_song`, `titre_song`, `annee_sortie`, `id_artiste`, `id_genre`) VALUES
(1, 'Space Oddity', 1969, 1, 'New wave - Rock'),
(2, 'China Girl', 1977, 1, 'New wave - Rock'),
(3, 'She\'s lost control', 1979, 2, 'Post-Punk '),
(4, 'Blowing the wind', 1963, 3, 'Blues - Rock - Country Folk'),
(5, 'Lovers in Japan', 2008, 4, 'Pop-rock'),
(6, 'Walk On ', 2000, 5, 'Rock Alternatif'),
(7, 'Forest of Jigramunt', 1995, 6, 'OST'),
(8, 'Ne comptez pas sur moi', 1991, 7, 'Punk Rock'),
(9, 'Pendons les haut et court', 1989, 7, 'Punk-Rock');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `class_artistes`
--
ALTER TABLE `class_artistes`
  ADD PRIMARY KEY (`id_artiste`);

--
-- Index pour la table `song_musique`
--
ALTER TABLE `song_musique`
  ADD PRIMARY KEY (`id_song`),
  ADD KEY `id_artiste` (`id_artiste`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `class_artistes`
--
ALTER TABLE `class_artistes`
  MODIFY `id_artiste` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `song_musique`
--
ALTER TABLE `song_musique`
  MODIFY `id_song` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

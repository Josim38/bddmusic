<?php require_once('connect.php') ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Songs</title>
    <link rel="stylesheet" href="">
</head>

<body>
    <?php
if(isset($_GET['idArt']))
$idArt=$_GET['idArt'];
else {
  $idArt=1;
}
$requete=$bdd->query("SELECT * FROM song_musique WHERE id_artiste ='$idArt'");
?>
        <table class="table table-responsive table-hover table-sm">
            <thead class="thead-default">
                <tr>
                    <th>id</th>
                    <th>Titre de la chanson</th>
                    <th>Année de sortie</th>
                    <th>Genre</th>
                </tr>
                <thead>
                    <?php
while ($a=$requete->fetch()) {
?>
                        <tbody>
                            <tr>
                                <td>
                                    <?php echo($a['id_song']) ?>
                                </td>
                                <td>
                                    <?php echo($a['titre_song']) ?>
                                </td>
                                <td>
                                    <?php echo($a['annee_sortie']) ?>
                                </td>
                                <td>
                                    <?php echo($a['id_genre']) ?>
                                </td>
                            </tr>
                        </tbody>
                        <?php } ?>
        </table>
        <?php $requete->closeCursor(); // Termine le traitement de la requête
  ?>
